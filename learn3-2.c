#include <stdio.h>
#include <math.h>
//存款的利息计算公式
int main(int argc, char const *argv[])
{
	float r5,r3,r2,r1,r0,p,p1,p2,p3,p4,p5;
	p=1000;
	r5=0.0585;
	r3=0.054;
	r2=0.0468;
	r1=0.0414;
	r0=0.0072;

	// 一次存5年期 
	p1=p*((1+r5)*5); 
	p2=p*((1+2*r2)*(1+3*r3));  //先存2年再存三年
	//先存3年在存2年；
	p3=p*((1+3*r3)*(1+2*r2));
	//先存1年，到期后再存1年 连续5次
	p4=p*pow(1+r1,5);
	//活期存款，美国季度结算一次
	p5=p*pow(1+r0/4,4*5);

	printf("p1=%f\n",p1 );
	printf("p2=%f\n",p2 );
	printf("p3=%f\n",p3);
	printf("p4=%f\n",p4 );
	printf("p5=%f\n",p5);
	return 0;
}