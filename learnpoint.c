#include <stdio.h>


	
int main() {
	int a = 5;
	int b = 3;
	void swap_2b(int a,int b);           //Can`t swap;
	void swap_normal(int *pa,int *pb);    //OK
	void swap_hack(int *pa,int *pb);      //OK
//	int a = 512;
//	int *p = &a;
//	printf("%d\n",*((unsigned char*)p));
//	return 0;
	
	
//int a,b,c,*p1,*p2,*p3,*p;
//	p1=&a;
//	p2=&b;
//	p3=&c; //指针变量赋值不应该带*
//	scanf("%d%d%d",p1,p2,p3);
//	if(*p1>*p2)
//	{ 
//		p=p1; //交换方法错误，这样才对
//		p1=p2;
//		p2=p;
//	} //保证a<=b
//	if(*p1>*p3)
//	{ 
//		p=p3;
//		p3=p1;
//		p1=p;
//	}//保证a<=c
//	if(*p2>*p3)
//	{ 
//		p=p2;
//		p2=p3;
//		p3=p;
//	}//保证b<=c  但这里并没有改变a b c 的值
//	printf("%d %d %d\n",*p1,*p2,*p3);
return 0;
}
void swap_2b(int a,int b){
	int t;
	t=a;
	a=b;
	b=t;
}
//程序员写的代码
void swap_normal(int *pa,int *pb){
	int t;
	t=*pa;
	*pa=*pb;
	*pb=t;
}
//黑客写的代码
void swap_hack(int *pa,int *pb){
	*pa = *pa^*pb;
	*pb = *pa^*pb;
	*pa = *pa^*pb;
}