#include <stdio.h>


struct date
{
	int year;
	int month;
	int day;
};
struct student
{
	char name[10];
	struct date birthday;
}student1;

int main() {
	struct student student1={
		"miming",
		1996,
		11,
		20
	};
	struct student student2;
	student2 = student1;
	printf("out print student1: %d,%d,%d,%d\n",student1.name,student1.birthday.year,student1.birthday.month,student1.birthday.day);
	printf("out print student2: %d,%d,%d,%d\n",student2.name,student2.birthday.year,student2.birthday.month,student2.birthday.day);
	return 0;
}