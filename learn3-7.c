#include <stdio.h>
//圆周长，圆面积，圆球表面积，圆球体积，圆柱体积
int main(int argc, char const *argv[])
{
	float r=1.5,h=3,l,s,ms,v,vq;
	float pi=3.141257;
    
    //圆周长
    l=2*pi*r; 
    //圆面积
    s=pi*r*r;
    //圆球表面积
    ms=4*pi*r*r;
    //圆球体积
    v=3.0/4.0*pi*r*r*r;
    //圆柱体积
    vq = pi*r*r*h;
    printf("圆周长为:       l=%6.2f\n",l);
    printf("圆周长为:       s=%6.2f\n",s);
    printf("圆周长为:       ms=%6.2f\n",ms);
    printf("圆周长为:       v=%6.2f\n",v);
    printf("圆周长为:       vq=%6.2f\n",vq);
	return 0;
}
