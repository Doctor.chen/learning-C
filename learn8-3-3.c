#include <stdio.h>
//给一个数组A,里面有10个元素，输出全部元素

/*下标法*/
// int main(int argc, char const *argv[])
// {
// 	int a[10];
// 	int i;
// 	printf("please enter 10 integer numbers:");
// 	for (int i = 0; i < 10; i++)
// 	{
// 		scanf("%d",&a[i]);
// 		/* code */
// 	}
// 	for (int i = 0; i < 10; i++)
// 	{
// 		/*下标法 数组元素用数组名和下标表示*/
// 		//printf("%d", a[i]);
// 		/*指针法 通过数组名和元素系列号技术元素地址*/
// 		printf("%d", *(a+i));
// 		/* code */
// 	}
// 	printf("\n");
// 	/* code */
// 	return 0;
// }

/*指针变量指向数组元素*/
int main(int argc, char const *argv[])
{
	int a[10];
	int *p,i;
	printf("please enter 10 integer numbers:");
	for (int i = 0; i < 10; i++)
	{
		scanf("%d",&a[i]);
	}
	for (p=a;p<(a+10);p++)
	{
		printf("%d",*p );
		/* code */
	}
	printf(" \n");
	return 0;
}