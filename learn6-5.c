#include <stdio.h>
//将一个数组中的值按逆序重新存放
#define N 5  //定义一个常量
int main(int argc, char const *argv[])
{
	int a[N],temp,i;
	printf("center array a:\n");

	for (int i = 0; i < N; i++)
	{
		scanf("%d",&a[i]);
		/* code */
	}
	printf("array a:\n");
	for (int i = 0; i < N; i++)
	{
		printf("%4d", a[i]);
		/* code */
	}
	//循环将元素的对称值互换
	for (int i = 0; i < N/2; i++)
	{
		temp =a[i];
		a[i]=a[N-i-1];
		a[N-i-1] = temp;
		/* code */
	}
	printf("\n Now,array is:\n");
	for (int i = 0; i < N; i++)
	{
		printf("%4d", a[i]);
		/* code */
	}
	printf("\n");

	return 0;
}